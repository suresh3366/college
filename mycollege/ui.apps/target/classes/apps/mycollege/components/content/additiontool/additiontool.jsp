<%--

  addition tool component.

  This is the component for adding 2 numbers

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %><%
%>

	<%	
    int sum=0;

    if(properties.get("N1","")!= "" && properties.get("N2","")!= "")  {

    String num1= properties.get("N1","");

    String num2 = properties.get("N2","");



    sum= Integer.parseInt(num1)+Integer.parseInt(num2);
    %>
    <h1> 1st number entered: <%= properties.get("N1","")%></h1>
	<h1> 2nd number entered: <%= properties.get("N2","")%></h1>
    <h1> Sum is : <%=sum%> <h1>

	<%  }
	else {
		out.print("Please provide two numbers to add");
	}
%>