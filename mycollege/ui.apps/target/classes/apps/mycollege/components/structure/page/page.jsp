<%--

  Page component.

  This is the component for page pages

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %>
 <cq:includeClientLib categories="cq.mycollege"/>
<cq:include path="sidekick" resourceType="/libs/wcm/core/components/init/init.jsp"/>

<div class="container_16">
    <div class="grid_8">
    <cq:include path="logo" resourceType="mycollege/components/structure/logo"/>
    </div>
    <cq:include path="topnav" resourceType="mycollege/components/structure/topnavigation"/>

    <div class="grid_16 body_container">
      <cq:include path="parsys" resourceType="foundation/components/parsys"/>
    </div>
    <div class="clear"></div>
</div>