package com.mycollege.core.service;


import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import java.util.Date;
@Component(name="My date service")

@Service(value=DateTime.class)
public class DateTime {
	
private String CurrentTime;
	
public String getCurrentTime()
{
	Date date = new Date();
	
	CurrentTime=String.valueOf(date);
	return CurrentTime;
			
}
}
